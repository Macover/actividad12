<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UsuarioController extends Controller
{


    public function login()
    {

        return view("login");

    }

    public function registro()
    {

        return view("registro");


    }

    public function perfil(){

        return view("profile");

    }

    public function registroForm(Request $datosFormulario)
    {

        $verificaCorreo = Usuario::where('correo', $datosFormulario->correo)->first();
        if ($verificaCorreo)
            return view("registro", ["estatus" => "error", "mensaje" => "¡El correo ya se encuentra registrado!"]);

        $nombreUsuario = $datosFormulario->nombreUsuario;
        $correo = $datosFormulario->correo;
        $password = $datosFormulario->password;

        $usuario = new Usuario();
        $usuario->nombre_usuario = $nombreUsuario;
        $usuario->correo =  $correo;
        $usuario->password = bcrypt($password);
        $usuario->save();
        return view("registro",["estatus"=> "success", "mensaje"=> "¡Cuenta Creada!"]);
    }

    public function verificarCredenciales(Request $datosFormulario){

        $usuario = Usuario::where("correo",$datosFormulario->correo)->first();
        if(!$usuario)
            return view("login",["estatus"=> "error", "mensaje"=> "¡El correo no esta registrado!"]);

        if(!Hash::check($datosFormulario->password,$usuario->password))
            return view("login",["estatus"=> "error", "mensaje"=> "¡Datos incorrectos!"]);

        Session::put('usuario',$usuario);

        if(isset($datosFormulario->url)){
            $url = decrypt($datosFormulario->url);
            return redirect($url);
        }else{
            return redirect()->route('profile.sucess');
        }

    }

    public function cerrarSesion(){
        if(Session::has('usuario'))
            Session::forget('usuario');

        return redirect()->route('login');
    }



}
